package xpug.kata.birthday_greetings;

public class Greetings {

	private String sender;
	private String receiver;
	private String body;
	private String subject;
	
	public Greetings(String sender, Employee employee) {
		this.sender = sender;
		this.receiver = employee.getEmail();
		this.body = "Happy Birthday, dear %NAME%!".replace("%NAME%", employee.getFirstName());
		this.subject = "Happy Birthday!";

	}
	
	public String getSender() {
		return sender;
	}

	public String getReceiver() {
		return receiver;
	}
	
	public String getBody() {
		return body;
	}

	public String getSubject() {
		return subject;
	}


}
