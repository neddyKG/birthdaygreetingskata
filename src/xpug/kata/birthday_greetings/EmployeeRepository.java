package xpug.kata.birthday_greetings;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface EmployeeRepository {
	public List<Employee> getEmployees() throws IOException, ParseException;
}