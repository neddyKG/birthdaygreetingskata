package xpug.kata.birthday_greetings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class FileEmployeeRepository implements EmployeeRepository {

	private String fileName;

	public FileEmployeeRepository(String fileName) {
		this.fileName = fileName;
	}
	
	public List<Employee> getEmployees() throws IOException, ParseException {
		System.out.println("Abriendo archivo");
		BufferedReader in = new BufferedReader(new FileReader(this.fileName));
		String str = "";
		str = in.readLine(); // skip header
		List<Employee> employees = new ArrayList<Employee>();
		
		System.out.println("Primera linea de archivo");
		while ((str = in.readLine()) != null) {
			String[] employeeData = str.split(", ");
			Employee employee = new Employee(employeeData[1], employeeData[0], employeeData[2], employeeData[3]);
			employees.add(employee);
		}
		return employees;
	}


}
