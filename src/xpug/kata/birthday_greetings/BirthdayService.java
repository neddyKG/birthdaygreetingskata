package xpug.kata.birthday_greetings;
import java.io.IOException;
import java.text.ParseException;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class BirthdayService {
	
	private EmployeeRepository repository;
	private EmailService mail;

	public BirthdayService(EmployeeRepository repository, EmailService mail) {
		this.repository = repository;
		this.mail = mail;
	}

	public void sendGreetings(OurDate ourDate) throws IOException, ParseException, AddressException, MessagingException {
			
			for(Employee employee : this.repository.getEmployees()){
				if (employee.isBirthday(ourDate)) {
					Greetings greeting = new Greetings("sender@here.com", employee);
					this.mail.sendMessage(greeting);
				}
			}
			
		}

	public static void main(String[] args) throws AddressException, IOException, ParseException, MessagingException {
		EmployeeRepository repository = new FileEmployeeRepository("employee_data.txt");
		EmailService mail = new SMTPMailService("localhost", 25); 
		
		BirthdayService service = new BirthdayService(repository, mail);
		service.sendGreetings(new OurDate("2008/10/08"));
		
	}

}
