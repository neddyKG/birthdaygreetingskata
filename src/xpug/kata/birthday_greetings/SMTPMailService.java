package xpug.kata.birthday_greetings;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SMTPMailService implements EmailService {

	private String smtpHost;
	private int smtpPort;

	public SMTPMailService(String smtpHost, int smtpPort) {
		this.smtpHost = smtpHost;
		this.smtpPort = smtpPort;
	}
	
	public void sendMessage(Greetings greeting) throws AddressException, MessagingException {
		Transport.send(createMessage(greeting));
	}

	private Message createMessage(Greetings greeting) throws MessagingException, AddressException {
		Message msg = new MimeMessage(createMailSession());
		msg.setFrom(new InternetAddress(greeting.getSender()));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(
				greeting.getReceiver()));
		msg.setSubject(greeting.getSubject());
		msg.setText(greeting.getBody());
		return msg;
	}

	private Session createMailSession() {
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", "" + smtpPort);
		Session session = Session.getDefaultInstance(props, null);
		return session;
	}


}
